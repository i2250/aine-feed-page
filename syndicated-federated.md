---
title: Syndicated / Federated
author: Colm O'Neill
theme: metropolis
institute: Institute of Technology Carlow
date: March 2022
---

# Do you know what this symbol means?

![](https://cdn2.iconfinder.com/data/icons/social-icon-3/512/social_style_3_rss-512.png)

# Syndicated

What does the word syndicated mean?

# Syndicated

syn·di·cate

1. An association of people or firms formed to promote a common interest

# RSS

Have you heard about RSS? 

What does it do?

Who is it for?

# What is the shape of the internet?

If you had to give the internet a shape, what would it be?

# A response from Louise Drulhe

[https://louisedrul.com/projects/the-two-webs/](https://louisedrul.com/projects/the-two-webs/)

# A global response

![](https://images.e-flux-systems.com/2016_06_01a_egypt-fiberoptics-e1465984509218.jpg,1125)

# Paul Baran Network diagrams

![](https://i0.wp.com/www.peacebiennale.info/blog/wp-content/uploads/2017/12/Fig-21.5-Baran_Diagrams-hi-res.jpg)

# THE OUTSIDE OF NETWORKS

THE OUTSIDE OF NETWORKS AS A METHOD FOR
ACTING IN THE WORLD

# Ulises Ali Mejias

Off the Network: Disrupting the Digital World (2013)


# THE OUTSIDE OF NETWORKS

IMAGINE A NETWORK MAP, with its usual nodes and links. Now shift your attention away
from the nodes, to the negative space between them. In network diagrams, the space
around a node is rendered in perfect emptiness, stillness, and silence. But this space is far
from barren. 

# THE OUTSIDE OF NETWORKS{.standout}

We can give a name to that which networks leave out, that which fills the
interstices between nodes with noise, and that which resists being assimilated by the
network: paranode.

# A paranode

![](https://images.e-flux-systems.com/2016_06_05_paranodeWEB.jpg,828)

# Federation

federated:

United by compact under a central organization, as governments or commercial organizations.

# Federated internet?

[https://fediverse.party/](https://fediverse.party/)